const firstArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const secondArray = ["1", "2", "3", "sea", "user", 23];
const thirdArray = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

function printArray(array, parent = document.body) {
  const ul = document.createElement("ul");
  array.map((item) => {
    if (Array.isArray(item)) {
      printArray(item, ul.lastElementChild);
    } else {
      let li = document.createElement("li");
      li.innerText = item;
      ul.append(li);
    }
  });

  parent.append(ul);
}

printArray(firstArray);

const taskSection = document.querySelector(".task");
printArray(secondArray, taskSection);
printArray(thirdArray, taskSection);

let seconds = 5;
const timer = document.createElement("h2");
const secondsSpan = document.createElement("span");
secondsSpan.innerText = String(seconds);
timer.append(secondsSpan, " seconds left");
document.body.prepend(timer);

setInterval(() => {
  seconds -= 1;
  secondsSpan.innerText = String(seconds);
}, 1000);

setTimeout(() => {
  document.body.innerText = "";
}, seconds * 1000);
